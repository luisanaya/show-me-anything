# README #

Utilizar un widget o un shortcode con placeholders para mostrar contenido en tus sitio web Wordpress. Encontrarás varias opciones para utilizar el widget, solo tienes que utilizar los placeholders en tu HTML personalizado y la información se mostrará como quieras sin tener conocimientos de PHP o Wordpress

### What is this repository for? ###

* ShowMeAnything
* Version: 0.1
* [Aprende más](https://bitbucket.org/tutorials/markdowndemo)

### Acerca de este repositorio ###


```
#!php

[show-me]... <div>mi html personalizado con los placeholders</div><h2>{TITULO}</h2> ...[/show-me]
```

Opciones:

* **{TITULO}** Muestra el título del post
* **{DESTACADA}** Muestra la imágen destacada
* **{EXTRACTO}** Muestra extracto del contenido
* **{FECHA}** Muestra la fecha del post
* **{COMENTARIOS}** Muestra la cantidad de comentarios al post
* **{LINK}** Muestra la url al post
* **{AUTOR_LINK}** Muestra la url de los post del autor
* **{AUTOR}** Muestra el nombre del autor
* **{CATEGORIAS_BLOG}** Muestra los nombres de las categorias del blog
* **{CATEGORIA_BLOG}** Muestra el nombre de la primer categoria asignada
* **{CATEGORIAS_BLOG_ENLAZADAS}** Muestra los nombres de las categorias del blog con sus respectivos enlaces
* **{CATEGORIA_BLOG_ENLAZADA}** Muestra el nombre de la primer categoria asignada con su respectivo enlace
* **{ETIQUETAS}** Muestra los nombres de las etiquetas en el post
* **{ETIQUETAS_ENLAZADAS}** Muestra los nombres de las etiquetas en el post con sus respectivos enlaces


Uso:

```
#!php

'limit' => 5,
'post_type' => 'post',
'taxonomy' => 'category',
'term' => '*'
```


### Acerca del desarrollador ###

* Autor: Luis Anaya | La Ã‰lite Web
* Contacto: luisanaya@laeliteweb.com
* www.laeliteweb.com