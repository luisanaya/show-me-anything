<?php
/*
Plugin Name: Show me anything
Description: Permite mostrar cualquier contenido de cualquier Custom Post type, de cualquier taxonomia en un widget
Version: 0.1
Author: Luis Anaya
Author URI: http://luisanaya.com
License: Creative Commons
*/

require_once 'functions.php';
/*
* Registro del widget en Wordpress
*/
add_action( 'widgets_init', 'lew_sma_register_widget' );
function lew_sma_register_widget() {
	register_widget('lew_show_me_anyting');
}

if (!class_exists('lew_show_me_anyting')) {
	class lew_show_me_anyting extends WP_Widget {
		function lew_show_me_anyting() {
			$widget_ops = array('classname' => 'lew_show_me_anyting', 'description' => 'Permite mostrar cualquier contenido de cualquier Custom Post type, de cualquier taxonomia en un widget');
			$control_ops = array('width' => 300, 'height' => 350);
			$this->WP_Widget('lew_show_me_anyting', 'Show Me Anything', $widget_ops, $control_ops);
			
			add_action( 'admin_enqueue_scripts', array($this, 'dinamic_taxonomies_adn_terms') );
		}

		function widget($args, $instance) {
			$lewf = new lew_show_me_anyting_functions;
			$args = array(
				'title' => $instance['title'],
				'template' => $instance['template'],
				'limit' => $instance['limit'],
				'post_type' => $instance['post_type'],
				'taxonomy' => $instance['taxonomy'],
				'term' => $instance['term'],
			);
			
			if( $args['term'] === '*' ){
				$terms = array();
				$slugs = $lewf->get_terms($args['taxonomy']);
				foreach( $slugs['slug'] as $term){
					$terms[] = $term;
				}
			} else { $terms = array($args['term']); }
			
			$qry = array(
				'post_type' => $args['post_type'],
				'posts_per_page' => (int)$args['limit'],
				'tax_query' => array(
					array(
						'taxonomy' => $args['taxonomy'],
						'field'    => 'slug',
						'terms'    => $terms
					)
				),
			);
			
			$query = new WP_Query( $qry );
			
			echo '<div class="widget">
				<div class="marn-barras-principales"><h4>'.$args['title'].'</h4></div>';
			if ( $query->have_posts() ) : 
				while ( $query->have_posts() ) : $query->the_post();
					global $post;
					echo html_entity_decode( $lewf->placeholders_replace($args['template'], $post->ID) );
				endwhile;
			endif;
			
			echo '</div>';
			wp_reset_postdata();
			wp_reset_query();
		}
		
		function form($instance) {
			$lewf = new lew_show_me_anyting_functions;
			$instance = wp_parse_args((array) $instance, array(
				'title' => 'Novedades',
				'post_type' => '',
				'taxonomy' => '',
				'term' => '',
				'template' => $lewf->template,
				'limit' => 5
			));
			$type['title'] = strip_tags($instance['title']);
			$type['post_type'] = strip_tags($instance['post_type']);
			$type['taxonomy'] = strip_tags($instance['taxonomy']);
			$type['term'] = strip_tags($instance['term']);
			$type['template'] = antiXSS($instance['template']);
			$type['limit'] = strip_tags($instance['limit']);
			$flew = new lew_show_me_anyting_functions;
			?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __('Titulo Widget', 'lew_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $type['title']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('url'); ?>"><?php echo __('Tipo de Post', 'lew_widget'); ?>:</label>
				<select class="widefat dinamic_post_type" id="<?php echo $this->get_field_id('post_type'); ?>" name="<?php echo $this->get_field_name('post_type'); ?>">
					<option>Selecciona un tipo de post</option>
					<?php echo $flew->get_formated_post_types($type['post_type']); ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php echo __('Taxonomia', 'lew_widget'); ?>:</label>
				<select data-ajax-selected = "<?php echo $type['taxonomy']; ?>" class="widefat dinamic_taxonomies" id="<?php echo $this->get_field_id('target'); ?>" name="<?php echo $this->get_field_name('taxonomy'); ?>">
					<?php echo $flew->get_formated_taxonomies($type['post_type'],$type['taxonomy']); ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('term'); ?>"><?php echo __('Termino', 'lew_widget'); ?>:</label>
				<select data-ajax-selected = "<?php echo $type['term']; ?>" class="widefat dinamic_terms" id="<?php echo $this->get_field_id('term'); ?>" name="<?php echo $this->get_field_name('term'); ?>">
					<?php echo $flew->get_formated_terms($type['taxonomy'],$type['term']); ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('template'); ?>"><?php echo __('Template', 'lew_widget'); ?>:</label>
				<textarea class="widefat" cols="20" rows="6" id="<?php echo $this->get_field_id('template'); ?>" name="<?php echo $this->get_field_name('template'); ?>"><?php echo html_entity_decode( $type['template'] ); ?></textarea>
				<div class="lew_help_format">
					<a href="" class="lew-toggle-help">¿Como utilizar?</a>
					<div class="lew_help" style="display:none;">
						<p>Puede utiliza los siguientes placeholder para aplica el formato que necesitas:</p>
						<ul>
							<li><strong>{TITULO}</strong> Muestra el título del post</li>
							<li><strong>{DESTACADA}</strong> Muestra la imágen destacada</li>
							<li><strong>{EXTRACTO}</strong>  Muestra extracto del contenido</li>
							<li><strong>{FECHA}</strong>  Muestra la fecha del post</li>
							<li><strong>{COMENTARIOS}</strong> Muestra la cantidad de comentarios al post</li>
							<li><strong>{LINK}</strong> Muestra la url al post</li>
							<li><strong>{AUTOR_LINK}</strong> Muestra la url de los post del autor </li>
							<li><strong>{AUTOR}</strong> Muestra el nombre del autor</li>
							<li><strong>{CATEGORIAS_BLOG}</strong> Muestra los nombres de las categorias del blog</li>
							<li><strong>{CATEGORIA_BLOG}</strong> Muestra el nombre de la primer categoria asignada</li>
							<li><strong>{CATEGORIAS_BLOG_ENLAZADAS}</strong> Muestra los nombres de las categorias del blog con sus respectivos enlaces</li>
							<li><strong>{CATEGORIA_BLOG_ENLAZADA}</strong> Muestra el nombre de la primer categoria asignada con su respectivo enlace</li>
							<li><strong>{ETIQUETAS}</strong> Muestra los nombres de las etiquetas en el post</li>
							<li><strong>{ETIQUETAS_ENLAZADAS}</strong> Muestra los nombres de las etiquetas en el post con sus respectivos enlaces</li>
						</ul>
					</div>
				</div>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('limit'); ?>"><?php echo __('Límite de publicaciones', 'lew_widget'); ?>:</label>
				<input type="number" class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>"  value="<?php echo $type['limit']; ?>" />
			</p>
			<?php
		}
		
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['post_type'] = strip_tags($new_instance['post_type']);
			$instance['taxonomy'] = strip_tags($new_instance['taxonomy']);
			$instance['term'] = strip_tags($new_instance['term']);
			$instance['template'] = antiXSS($new_instance['template']);
			$instance['limit'] = strip_tags($new_instance['limit']);
			return $instance;
		}
		
		/*
		* Funcionalidad ajax para las select de los widgets
		*/
		function dinamic_taxonomies_adn_terms(){
			//Encolamos jquery, en un principio no detectaba jquery, opte por encolar un cdn
			?>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script>
			/*
			* Developed by Luis Anaya
			* www.luisanaya.com
			*/
			jQuery(document).ready(function(e) {
				
				jQuery('body').delegate('.lew-toggle-help','click',function(){
					jQuery(this).siblings('.lew_help').slideToggle(800);
					return false;
				});
				
				jQuery('body').delegate('.dinamic_post_type','click change',function(){
					var post_type = jQuery(this);
					var selected = jQuery('.dinamic_post_type').parent().parent().find('select.dinamic_taxonomies').attr('data-ajax-selected');
					get_taxonomy(post_type, post_type.val(), selected);
				});
				
				jQuery('body').delegate('.dinamic_taxonomies','click change',function(){
					var taxonomy = jQuery(this);
					var selected = jQuery('.dinamic_taxonomies').parent().parent().find('select.dinamic_terms').attr('data-ajax-selected');
					get_terms(taxonomy, taxonomy.val(), selected);
				});
				
				function get_taxonomy(element, post_type, selected){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo admin_url('admin-ajax.php'); ?>",
						data: {post_type:post_type,taxonomy:selected,action:'get_taxonomies_from_post_type'},
						success: function(data){
							jQuery(element).parent().parent().find('select.dinamic_taxonomies').html(data);
						}
					});
				}
				
				function get_terms(element, taxonomy, selected){
					jQuery.ajax({
						type: "POST",
						url: "<?php echo admin_url('admin-ajax.php'); ?>",
						data: {taxonomy:taxonomy,term:selected,action:'get_terms_from_taxonomy'},
						success: function(data){
							jQuery(element).parent().parent().find('select.dinamic_terms').html(data);
						}
					});
				}
			});
			</script>
			<?php
		}
		
	}
	
}

/*
* Con esta función ajax devolveremos la miniatura de la imagen que se está asignando en vivo
*/
function get_taxonomies_from_post_type(){
	$post_type = strip_tags($_POST['post_type']);
	$selected = strip_tags($_POST['taxonomy']);
	$lewf = new lew_show_me_anyting_functions;
	echo $lewf->get_formated_taxonomies($post_type,$selected);
	die();
}
add_action('wp_ajax_get_taxonomies_from_post_type','get_taxonomies_from_post_type');

function get_terms_from_taxonomy(){
	$taxonomy = strip_tags($_POST['taxonomy']);
	$selected = strip_tags($_POST['term']);
	$lewf = new lew_show_me_anyting_functions;
	echo $lewf->get_formated_terms($taxonomy,$selected);
	die();
}
add_action('wp_ajax_get_terms_from_taxonomy','get_terms_from_taxonomy');

add_shortcode('show-me','show_me_enything');
function show_me_enything($atts,$content){
	$atributes = shortcode_atts(array(
		'limit' => 5,
		'post_type' => 'post',
		'taxonomy' => 'category',
		'term' => '*'
		), $atts );
	
	$lewf = new lew_show_me_anyting_functions;
	
	if( $atributes['term'] === '*' ){
		$terms = array();
		$slugs = $lewf->get_terms($atributes['taxonomy']);
		foreach( $slugs['slug'] as $term){
			$terms[] = $term;
		}
	} else { $terms = array($atributes['term']); }
		
	$qry = array(
		'post_type' => $atributes['post_type'],
		'posts_per_page' => (int)$atributes['limit'],
		'tax_query' => array(
			array(
				'taxonomy' => $atributes['taxonomy'],
				'field'    => 'slug',
				'terms'    => $terms
			)
		),
	);
	
	$query = new WP_Query( $qry );
	$out = '<div class="widget">';
	if ( $query->have_posts() ) : 
		while ( $query->have_posts() ) : $query->the_post();
			global $post;
			$contenido = html_entity_decode( $lewf->placeholders_replace($content, $post->ID) );
			$out .= do_shortcode($contenido);
		endwhile;
	endif;
	$out .= '</div>';
	wp_reset_postdata();
	wp_reset_query();
	
	return $out;
}