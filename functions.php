<?php
/*
* Autor: Luis Anaya
* http://luisanaya.com/
*/

if (!class_exists('lew_show_me_anyting_functions')) {
	class lew_show_me_anyting_functions {
		public $post_type;
		public $template = '
		<div class="widget-post">
			<h1 class="widget-post-title"><a href="{LINK}">{TITULO}</a></h1>
			<div class="widget-post-imagen-destacada">{DESTACADA}</div>
			<div class="widget-post-contenido">{EXTRACTO}</div>
			<div class="sidget-post-meta-data">
				<span>{FECHA}</span>
				<span>Comentarios: {COMENTARIOS}</span>
				<br>
				<span>Autor: <a href="{AUTOR_LINK}">{AUTOR}</a></span>
				<br>
				<span>Etiquetas: {ETIQUETAS}</span>
				<span>Categorias: {CATEGORIAS_BLOG}</span>
			</div>
			<a href="{LINK}">Leer completo</a>
		</div>';
		
		public function __construct(){}
		
		/*
		* Obtengo los post_type registrados en WordPress en formato de array
		* return Array[]
		*/
		public function get_post_types(){
			$out = array();
			$post_types = get_post_types( '', 'names' );
			
			foreach ( $post_types as $post_type ) {
			   $out[] = $post_type;
			}
			
			return $out;
		}
		
		/*
		* Obtengo las taxonomias de un post_type registrados en WordPress en formato de array
		* return Array[]
		*/
		public function get_taxonomies($post_type){
			$post_type = strip_tags($post_type);
			if( empty($post_type) ){
				return;
			}
			
			$out = array();
			$taxonomy_objects = get_object_taxonomies( $post_type, 'objects' );
			
			foreach ( $taxonomy_objects as $taxonomy ) {
				$out['label'][] = $taxonomy->label;
				$out['name'][] =$taxonomy->name;
			}
			
			return $out;
		}
		
		/*
		* Obtengo los terms de una taxonomias registrados en WordPress en formato de array
		* return Array[]
		*/
		public function get_terms($taxonomy_name){
			$taxonomy_name = strip_tags($taxonomy_name);
			if( empty($taxonomy_name) ){
				return;
			}
			
			$out = array();
			$terms = get_terms($taxonomy_name);
			
			foreach ( $terms as $term ) {
				$out['slug'][] = $term->slug;
				$out['name'][] =$term->name;
			}
			
			return $out;
		}
		
		/*
		* Aplicamos unos placeholder a un template personalizado pasado como argumento
		* return String, post con formato html
		*/
		public function placeholders_replace($template,$id) {
			if( empty($template) || $template === '' ){
				$template = $this->template;
			}
			$data = array(
				'titulo' => get_the_title($id),
				'extracto' => get_the_excerpt($id),
				'link' => get_the_permalink($id),
				'destacada' => self::get_featured_image($id),
				'comentarios' => get_comments_number($id),
				'fecha' => get_the_time(get_option('date_format'), $id),
				'autor_link' => self::get_the_author_link($id),
				'autor' => self::get_the_author_name($id),
				'categorias_blog' => self::get_categories_blog($id),
				'categoria_blog' => self::get_first_category_blog($id),
				'categorias_blog_enlazadas' => self::get_categories_blog_link($id),
				'categoria_blog_enlazada' => self::get_first_category_blog_link($id),
				'etiquetas' => self::get_the_tags($id),
				'etiquetas_enlazadas' => self::get_the_tag_link($id),
			);
			
			$placeholders = array_keys($data);
			
			foreach ($placeholders as &$placeholder) {
				$placeholder = strtoupper("{{$placeholder}}");
			}
			
			return str_replace($placeholders, array_values($data), $template);
		}
		
		private function get_featured_image($id){
			if( has_post_thumbnail() ){
				return get_the_post_thumbnail($id,'medium');
			} else {
				return '<img src="'.plugin_basename(__FILE__).'/images/default.png" />';
			}
		}
		
		/*
		* Obtengo la url de los post del autor
		* return la url como tal
		* ejemplo: http://miweb.com/author/admin/
		*/
		private function get_the_author_link($id){
			$post = get_post( $id );
			return get_author_posts_url( get_the_author_meta( 'ID', $post->post_author ) );				
		}
		
		/*
		* Obtengo el nombre del autor
		* return el nombre configurado como display en las opciones de usuarios
		* ejemplo: admin | Admin Perez | Juan Perez | juanperez
		*/
		private function get_the_author_name($id){
			$post = get_post( $id );
			return get_the_author_meta( 'display_name', $post->post_author );
		}
		
		/*
		* Obtengo las categorias del post
		* return names
		* ejemplo: Sin Categoría
		*/
		private function get_categories_blog($id){
			$categories = get_the_category($id);
			$categorias = array();
			if( is_array($categories) ){
				foreach($categories as $category){
					$categorias[] = $category->name;
				}
				return implode(', ',$categorias);
			} else return '';
		}
		
		/*
		* Obtengo las categorias del post con su respectivo enlace
		* return links and names
		* ejemplo: <a href="http://miweb.com/category/category-name/">Category Name</a>
		*/
		private function get_categories_blog_link($id){
			$categories = get_the_category($id);
			$categorias = array();
			if( is_array($categories) ){
				foreach($categories as $category){
					$categorias[] = '<a href="'.esc_url(get_category_link( $category->term_id )).'">'.$category->name.'</a>';
				}
				return implode(', ',$categorias);
			} else return '';
		}
		
		/*
		* Obtengo sólo la primer categoria del post
		* return name
		* ejemplo: Category Name
		*/
		private function get_first_category_blog($id){
			$category = get_the_category($id); 
			return $category[0]->cat_name;
		}
		
		/*
		* Obtengo sólo la primer categoria del post con su respectivo enlace
		* return link and name
		* ejemplo: <a href="http://miweb.com/category/category-name/">Category Name</a>
		*/
		private function get_first_category_blog_link($id){
			$category = get_the_category($id); 
			return '<a href="'.esc_url(get_category_link( $category[0]->term_id )).'">'.$category[0]->cat_name.'</a>';
		}
		
		/*
		* Obtengo las etiquetas del post
		* return names
		* ejemplo: Tag 1, Tag 2, Tags 3, etc
		*/
		private function get_the_tags($id){
			$tags = get_the_tags($id);
			$etiquetas = array();
			if( is_array($tags) ){
				foreach($tags as $tag){
					$etiquetas[] = $tag->name;
				}
				return implode(', ',$etiquetas);
			} else return '';
		}
		
		/*
		* Obtengo las etiquetas del post con su respectivo enlace
		* return links and names
		* ejemplo: <a href="http://miweb.com/tags/tag-1/">Tag 1</a>
		*/
		private function get_the_tag_link($id){
			$tags = get_the_tags($id);
			$etiquetas = array();
			if( is_array($tags) ){
				foreach($tags as $tag){
					$etiquetas[] = '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
				}
				return implode(', ',$etiquetas);
			} else return '';
		}
		
		/*
		* Obtengo los post_type registrados en WordPress en formato de options de un select
		* return String <option>
		*/
		public function get_formated_post_types($selected = ''){
			$post_types = self::get_post_types();
			$out = array();
			
			foreach($post_types as $pt){
				$out[] = '<option value="'.$pt.'" '.selected($pt,$selected,false).'>'.ucfirst($pt).'</option>';
			}
			
			return implode('',$out);
		}
		
		/*
		* Obtengo las taxonomias de un post_type registradas en WordPress en formato de options de un select
		* return String <option>
		*/
		public function get_formated_taxonomies($post_type, $selected = ''){
			$post_type = strip_tags($post_type);
			$taxonomies = self::get_taxonomies($post_type);
			$out = array('<option value="*">Todas</option>');
			
			$count = 0;
			foreach($taxonomies as $tx){
				$out[] = '<option value="'.$taxonomies['name'][$count].'" '.selected($taxonomies['name'][$count],$selected,false).'>'.ucfirst($taxonomies['label'][$count]).'</option>';
				$count++;
			}
			
			return implode('',$out);
		}
		
		/*
		* Obtengo los terminos de una taxonomia registrados en WordPress en formato de options de un select
		* return String <option>
		*/
		public function get_formated_terms($taxonomy, $selected = ''){
			$taxonomy = strip_tags($taxonomy);
			$terms = self::get_terms($taxonomy);
			$out = array('<option value="*">Todas</option>');
			
			for($i = 0; $i < count($terms['slug']); $i++){
				$out[] = '<option value="'.$terms['slug'][$i].'" '.selected($terms['slug'][$i],$selected,false).'>'.ucfirst($terms['name'][$i]).'</option>';
			}
			
			return implode('',$out);
		}
	}
}
